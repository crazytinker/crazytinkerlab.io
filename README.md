# Crazy Tinker

> "The first rule of intelligent tinkering is to save all the parts." - [Paul R. Ehrlich](https://en.wikipedia.org/wiki/Paul_R._Ehrlich)

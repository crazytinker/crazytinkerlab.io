var repos = {

	location: "https://gitlab.com/api/v4/groups/crazytinker/projects",

	fetch: function() { 
		var request = new XMLHttpRequest();

		request.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				repos.list = JSON.parse(this.responseText),
					nav = document.getElementsByClassName('bs-sidenav')[0];

				for (i = 0; i < repos.list.length; i++) {
					var project = repos.list[i];

					projects = document.createElement('div');
					projects.innerHTML += "<a id='#" + project.name.toLowerCase() + "' href='" + project.web_url + "'>" + titleCase(project.name) + "</a>";
					if (project.description != null) {
						projects.innerHTML += "<p class='card-text'>" + project.description + "</p>";
					}
					document.getElementById('git-repos').appendChild(projects);

					nav.innerHTML += "<li><a href='#" + project.name.toLowerCase() + "'>" + titleCase(project.name) + "</a></li>";
				}
			}
		}

		request.open("GET", this.location, true);
		request.send();
	}
}

function titleCase(str) {
	return str.split(' ')
		.map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
		.join(' ');
}

window.onload = repos.fetch();

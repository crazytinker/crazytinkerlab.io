# Crazy Tinker
"The first rule of intelligent tinkering is to save all the parts." - [Paul R. Ehrlich](https://en.wikipedia.org/wiki/Paul_R._Ehrlich)

<div id="git-repos">
  <!-- Dynamically populated via Javascript -->
</div>

<script type="text/javscript" src="js/repos.js"></script>

<style>
.navbar-nav {
  display: none;
  visibility: hidden;
}
#git-repos > div {
  padding: 10px;
}
</style>
